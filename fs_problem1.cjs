const fs = require("fs");
module.exports=createAndDeleteDirectoryAndFiles
function createAndDeleteDirectoryAndFiles(path, noOfFiles) {
  // console.log(path);
  if (!fs.existsSync(path)) {
    fs.mkdir(path, { recursive: true }, (err) => {
      if (err) {
        console.log(err);
      } else {
        createFilesInDirectory(path + "/", noOfFiles, () => {
          deletefilesAndDirectory(path + "/", noOfFiles);
        });
      }
    });
  } else {
    createFilesInDirectory(path + "/", noOfFiles, () => {
      deletefilesAndDirectory(path + "/", noOfFiles);
    });
  }
}

function createFilesInDirectory(path, noOfFiles, callback) {
  let count = noOfFiles;

  for (let index = 0; index < noOfFiles; index++) {
    count--;
    let fileNo = index + 1;
    fs.writeFileSync(
      path + `file${fileNo}.json`,
      "demo content for file",
      (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("file" + fileNo + " created");
        }
      }
    );

    if (count == 0) {
      console.log("All files created now we will call delete function");
      callback();
    }
  }
}

function deletefilesAndDirectory(path, noOfFiles) {
  let count = noOfFiles;
  for (let index = 0; index < noOfFiles; index++) {
    count--;
    let fileNo = index + 1;
    fs.unlink(path + `file${fileNo}.json`, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("file" + fileNo + " deleted");
      }
    });
    if (count == 0) {
      fs.rmdir(path, { recursive: true }, (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("directory deleted");
        }
      });
    }
  }
}
