/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");
// const data=require("./lipsum1.txt")
const path = require("path");

function readData(path) {
  fs.readFile(path, "utf8", (err, data) => {
    if (err) {
      console.log("err");
    } else {
      convertTextUppercase(data, convertTextLowerCase);
    }
  });
}

function convertTextUppercase(data) {
  let newdata = data.toUpperCase();

  const filePath = path.join(__dirname, "/upper_case.txt");

  fs.writeFile(filePath, newdata, (err) => {
    if (err) {
      console.log(err.message);
    } else {
      const filePathforAppend = path.join(__dirname, "/fileName.txt");
      fs.appendFile(filePathforAppend, "upper_case.txt" + "\n", (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("name appended upper case");
        }
      });
    }
  });
  convertTextLowerCase(path);
}

function convertTextLowerCase(data) {
  let filepath = "/home/asjad/ubuntu/vs code files/call_back/lipsum_1.txt";
  fs.readFile(filepath, "utf8", (err, data) => {
    if (err) {
      console.log("err");
    } else {
      let newData = data.toLocaleLowerCase();
      let SplittedData = newData.split(". ");
      const filePath = path.join(__dirname, "/lower_case.txt");
      SplittedData.forEach((sentence) => {
        fs.appendFile(filePath, sentence + "\n", (err) => {
          if (err) {
            console.log(err.message);
          }
        });
      });
      const filePathforAppend = path.join(__dirname, "/fileName.txt");
      fs.appendFile(filePathforAppend, "lower_case.txt" + "\n", (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("name appended for lower case");
          readSortFile(
            "/home/asjad/ubuntu/vs code files/call_back/lipsum_1.txt"
          );
        }
      });
    }
  });
}

function readSortFile(path) {
  fs.readFile(path, "utf8", (err, data) => {
    if (err) {
      console.log("err");
    } else {
      let splittedData = data.split(" ");
      splittedData.sort();
      const filePath =
        "/home/asjad/ubuntu/vs code files/call_back/sorted_word.txt";
      splittedData.forEach((word) => {
        fs.appendFile(filePath, word + "\n", (err) => {
          if (err) {
            console.log(err.message);
          }
        });
      });
      const filePathforAppend =
        "/home/asjad/ubuntu/vs code files/call_back/fileName.txt";
      fs.appendFile(filePathforAppend, "sorted_word.txt" + "\n", (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("name appended for sorted file");
          deletefilesAndDirectory(
            "/home/asjad/ubuntu/vs code files/call_back/fileName.txt"
          );
        }
      });
    }
  });
}

function deletefilesAndDirectory(path) {
  console.log(path);

  fs.readFile(path, "UTF8", (err, data) => {
    if (err) {
      console.log(err.message);
    } else {
      //   console.log(data);
      let filenameArr = data.split("\n");
      console.log(filenameArr);
      filenameArr.forEach((filename) => {
        if (fs.existsSync(filename)) {
          fs.unlink(filename, (err) => {
            if (err) {
              console.log(err);
            } else {
              console.log(filename + " deleted");
            }
          });
        }
      });
    }
  });

  // const filePath = "/home/asjad/ubuntu/vs code files/call_back/fileName.txt";
  // fs.writeFile(filePath, "", "utf8", (err) => {
  //   if (err) {
  //     console.error("Error writing to file:", err);
  //   }else{
  //     console.log("removed file names from filename txt")
  //   }

  // });
}

module.exports = readData;
